<?php
class Image
{
    private $obj_data;
    private $imageid;
    public function __construct($src_obj=0)
    {
        BD::getInstance();
        $this->obj_data = array();
        $this->obj_data['imageid'] = 0;
        if (is_array($src_obj))
        {
            $this->setId(intval($src_obj['imageid']));
            $this->setData($src_obj['userid'],$src_obj['src'],$src_obj['created_at']);
        }
        elseif (is_numeric($src_obj))
        {
           $sql = 'SELECT * FROM images WHERE imageid='.intval($src_obj);
           $res = BD::get($sql);
           $this->obj_data = $res[0];
           $this->setId(intval($res[0]['imageid']));
           $this->setData($res[0]['userid'],$res[0]['src'],$res[0]['created_at']);
        }
    }

    public function getImageId()
    {
      return $this->obj_data['imageid'];
    }

    public function setId($id=0)
    {
        $this->obj_data['imageid'] = intval($id);
    }

    public function setData($userid,$src,$created_at=-1)
    {
       $created_at_time = strtotime($created_at);
       if ($created_at_time < 0) $created_at_time = time();
       $this->obj_data['userid']    = intval($userid);
       $this->obj_data['created_at']=$created_at_time;
       $this->obj_data['src']       = $src;
    }
    public function getData()
    {
       return $this->obj_data;
    }
    public function store()
    {
       $src    = mysql_escape_string($this->obj_data['src']);
       $created_at_time_str = date('Y-m-d H:i:s',$this->obj_data['created_at']);
       if ($this->obj_data['imageid'] > 0)
       {
           $sql = 'UPDATE images SET userid="'.$this->obj_data['userid'].'", 
                                        src="'.$src.'", 
                                 created_at="'.$created_at_time_str.'"
                                        WHERE 
                                        imageid='.$this->obj_data['imageid'].'
                                       ';
           BD::query($sql);
           return $this->obj_data['imageid'];
       }
       $sql = 'INSERT DELAYED INTO images (userid,src,created_at)  VALUES ( "'.$this->obj_data['userid'].'",  "'.$src.'",  "'.$created_at_time_str .'");';
       return BD::insert($sql);

    }
}