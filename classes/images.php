<?php
class Images
{
    private $bd_obj;
    public function __construct()
    {
       BD::getInstance();
    }
    public function getCount()
    {
        $sql = 'SELECT COUNT(imageid) AS cnt FROM images';
        $res = BD::get($sql);
        return $res[0]['cnt'];
    }
    public function getList($start,$limit)
    {
        $sql = 'SELECT * FROM images ORDER BY imageid LIMIT '.intval($start).','.intval($limit);
        return BD::get($sql);
    }

    public function getListIds($start,$limit)
    {
        $sql = 'SELECT imageid FROM images ORDER BY imageid LIMIT '.intval($start).','.intval($limit);
        return BD::get($sql);
    }

    public function getCountByParams($allow_tags,$ignore_tag)
    {
        $sql = 'SELECT imagetags.imageid
                FROM imagetags
                LEFT JOIN images ON imagetags.imageid = images.imageid
                WHERE 1  ';
        if (count($allow_tags))
        {
            $sql .= 'AND  imagetags.tagid IN ('.join($allow_tags,',').') ';
        }
       
        if (count($ignore_tag))
        {
	    $igsql = ' AND images.imageid  NOT  IN (SELECT imageid FROM imagetags WHERE tagid IN ('.join( $ignore_tag,',').'))';
            if (count($allow_tags)==0)
            {
              $igsql = ' AND  imagetags.tagid NOT IN ('.join( $ignore_tag,',').')';
            }
            $sql .=$igsql;
        }
        $sql .='GROUP BY imagetags.imageid HAVING COUNT( imagetags.tagid ) ='.count($allow_tags).' ';
        //die($sql);
        
        if (count($allow_tags)==0 and  count($ignore_tag) ==0)
        {
            $sql = 'SELECT COUNT(imageid) AS cnt FROM images';
        }
        $res =  BD::get($sql);
        return $res[0]['cnt'];
    }

    public function getListByParams($start,$limit,$allow_tags,$ignore_tag,$sort_by_date,$sort_by_likes)
    {
        $sql = 'SELECT  *  
                  FROM imagetags 

                  LEFT JOIN images ON imagetags.imageid = images.imageid 
                  LEFT JOIN likes ON likes.imageid = images.imageid
                  WHERE  1 ';
        if (count($allow_tags))
        {
            $sql .= 'AND  imagetags.tagid IN ('.join($allow_tags,',').') ';
        }
        if (count($ignore_tag))
        {
           
	    $igsql = ' AND images.imageid  NOT  IN (SELECT imageid FROM imagetags WHERE tagid IN ('.join( $ignore_tag,',').'))';
            if (count($allow_tags)==0) 
            {
                $igsql = ' AND  imagetags.tagid NOT IN ('.join($ignore_tag,',').') ';
            }
            $sql .=   $igsql;
        }
        $order_sql = '';
        if ($sort_by_date)
        {
            $order_sql = ' ORDER BY images.created_at ';
        }
        if ($sort_by_likes)
        {
            $order_sql = ' ORDER BY likes.cnt DESC ';
        }
	$sql .=$order_sql;
        $sql .=' GROUP BY imagetags.imageid HAVING COUNT( imagetags.tagid ) ='.count($allow_tags).' ';
        $sql .=  ' LIMIT '.intval($start).','.intval($limit);
        //die($sql);
        $res =  BD::get($sql);
        return $res;
    }

    public function store($userid,$src,$created_at)
    {
         $im = new Image($this->bd_obj);
         $im->setData($userid,$src,$created_at);
         $im->store();
    }
}
