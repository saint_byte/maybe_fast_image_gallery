<?php
function __autoload($class_name) {
    $class_name = strtolower($class_name);
    $path = __DIR__.'/'.$class_name.'.php';
    if (!file_exists($path))
    {
       die('Class:'.$class_name. ' in file '.$path. 'NOT FILE');
    }
    include_once($path);
}