<?php
class Likes
{
    private $_imageid;
    public function __construct()
    {
        BD::getInstance();
    }
    
    public function setImage($imageid)
    {
        $this->_imageid=$imageid;
    }
    public function setLikeCount($count)
    {
        $sql = 'REPLACE INTO likes SET 	imageid="'.$this->_imageid.'",cnt="'.$count.'"';
        BD::query($sql);
    }
    public function getByImageId($imageid)
    {
       $sql = 'SELECT cnt FROM likes WHERE imageid='.$imageid;
       $res = BD::get($sql);
       return $res[0]['cnt'];
    }
}