<?php
class Tags
{
    public function __construct()
    {
             BD::getInstance();
    }
    /**
    * Нормализация строка для дальнейшего использования как тега
    * @param string Ненормализования строка
    */
    public function normalize_tag_string($tag)
    {
       $tag = trim($tag);
       $tag = mb_strtolower($tag,'UTF-8');
       return $tag;
    }
    public function exists($tag)
    {
        $tag=$this->normalize_tag_string($tag);
        $tag=mysql_escape_string($tag);
        $sql = 'SELECT tagid FROM  tags WHERE  tag="'.$tag.'"';
        $res = BD::get($sql);
        if ($res) return true;
        return false;
    }
    public function getTagByName($name)
    {
        $tag=$this->normalize_tag_string($tag);
        $tag=mysql_escape_string($tag);
        $sql = 'SELECT  *  FROM  tags WHERE  tag="'.$tag.'"';
        $res = BD::get($sql);
        if ($res) return $res[0];
        return false;
    }
    
    public function getAll()
    {
        $sql = 'SELECT * FROM tags';
        return BD::get($sql);
    }
    
    public function getByImageId($imageid)
    {
      $sql = 'SELECT tags.tagid,tags.tag FROM  imagetags LEFT JOIN tags ON imagetags.tagid = tags.tagid WHERE imageid ='.$imageid;
      return BD::get($sql);
    }
    public function store($tag)
    {
        $tag=$this->normalize_tag_string($tag);
        if ($this->exists($tag))
        {
             return false;
        }
        $tag=mysql_escape_string($tag);
        return BD::insert('INSERT INTO tags(tag) VALUES("'.$tag.'")');
    }
}