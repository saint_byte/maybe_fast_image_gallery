<?php
class ImageTags
{
    private $_imageid;
    private $_tagids = array();
    public function __construct()
    {
        BD::getInstance();
    }
    public function setImage($image)
    {
       if (is_numeric($image))
       {
          $this->_imageid = $image;
       }
    }
    /**
    * Устанавливаем таги картинке
    * @param array Массив тагов 
    * @param string(tagnames|tagids) Типа массива тагов , (так как вдруг таги цифирки =) )
    *                   
    */
    public function setTags($arr,$type='tagnames')
    {
        if ($type == 'tagids')
        {
           $this->_tagids = $arr;
        }
        if ($type == 'tagnames')
        {
           //$this->_tagids = $arr;
        }

    }
    
    public function store()
    {
        // Наиболее простой способ =)
        $sql = 'DELETE FROM imagetags WHERE imageid='.intval($this->_imageid);
        BD::query($sql);
        foreach($this->_tagids as $tagid)
        {
            $sql = 'INSERT DELAYED INTO imagetags(imageid,tagid) VALUES('.$this->_imageid.','.$tagid.')';
            BD::query($sql);
        }
    }
}