<?php
class BD
{
    protected static $_instance;
    protected static $_link;
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self(Config::getConfig());
        }
        return self::$_instance;
    }

    public function __clone()
    {
         throw new Exception("Can't clone a singleton");
    }

    public function __construct($_config)
    {
	$link= mysql_connect($_config['mysql_host'],$_config['mysql_login'],$_config['mysql_password']);
        self::$_link=$link;
	if (!$link) {
	    die('Ошибка соединения: ' . mysql_error());
	}
	mysql_selectdb($_config['mysql_db'], self::$_link);
        mysql_query($_config['mysql_init_sql'], self::$_link);
    }
    
    

    /**
    * простой запрос к базе данные , без резултата
    */
    public static function query($sql)
    {
      mysql_query($sql,self::$_link);
      if (mysql_errno())
      {
            print "MySQL error ".mysql_errno().": ".mysql_error()."\n";
            debug_print_backtrace();
      }
    }
    /**
    * Получить данные из базы
    */
    public static function get($sql)
    {
        $qh  = mysql_query($sql,self::$_link);
        if (mysql_errno())
        {
            print "MySQL error ".mysql_errno().": ".mysql_error()."\n";
            debug_print_backtrace();
        }
        $cnt =  mysql_num_rows($qh);
        if ($cnt == 0) return false;
        $result = array();
        while ($row = mysql_fetch_array($qh, MYSQL_ASSOC)) 
        {
            $result[] = $row;
        }
        return $result;
     }
     /**
     * 
     *
     */
     public static function insert($sql)
     {
         $qh  = mysql_query($sql,self::$_link);
         if (mysql_errno())
         {
            print "MySQL error ".mysql_errno().": ".mysql_error()."\n";
            debug_print_backtrace();
         }
         $res = mysql_insert_id(self::$_link);
         return $res;
     }
}  