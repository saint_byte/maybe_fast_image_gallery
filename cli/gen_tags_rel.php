#!/usr/bin/php
<?php
include('../config.php');
include('../classes/autoload.php');
$bd   = new BD($_config);
$tags = new Tags();
$all_tags_arr = $tags->getAll();
$all_tags_arr_cnt = count($all_tags_arr);
$ims  = new Images();
$cnt  = $ims->getCount();
$per_one = 20000;
for($i=0; $i < $cnt; $i = $i+$per_one)
{
    $images = $ims->getListIds($i,$per_one);
    foreach($images as $image)
    {
      $tags_cnt = intval(rand(3,6));
      $tagids = array();
      $its = new ImageTags();
      $its->setImage($image['imageid']);
      for($i2=0;$i2<$tags_cnt;$i2++)
      {
         $tagids[] = $all_tags_arr[rand(0,$all_tags_arr_cnt-1)]['tagid'];
      }
      $its->setTags($tagids,'tagids');
      $its->store();
    }
    print $i."\r\n";
}
print date('C');