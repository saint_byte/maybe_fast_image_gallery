#!/usr/bin/php
<?php
include('../config.php');
include('../classes/autoload.php');
$bd   = new BD($_config);
$tags = new Tags();
$all_tags_arr = $tags->getAll();
$all_tags_arr_cnt = count($all_tags_arr);
$ims  = new Images();
$cnt  = $ims->getCount();
$per_one = 20000;
for($i=0; $i < $cnt; $i = $i+$per_one)
{
    $images = $ims->getListIds($i,$per_one);
    foreach($images as $image)
    {
      $likes = intval(rand(0,1000));
      $l = new Likes();
      $l->setImage($image['imageid']);
      $l->setLikeCount($likes);
    }
    print $i."\r\n";
}
print date('C');