-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 07, 2014 at 12:41 PM
-- Server version: 5.5.22-0ubuntu1
-- PHP Version: 5.3.10-1ubuntu3.11

SET time_zone = "+00:00";

--
-- Database: `nv`
--
CREATE DATABASE IF NOT EXISTS `nv` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE nv;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `imageid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `src` varchar(255) binary NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`imageid`),
  KEY `userid` (`userid`,`created_at`),
  KEY `created_at` (`created_at`)
) TYPE=MyISAM  AUTO_INCREMENT=1000000 ;

-- --------------------------------------------------------

--
-- Table structure for table `imagetags`
--

DROP TABLE IF EXISTS `imagetags`;
CREATE TABLE IF NOT EXISTS `imagetags` (
  `imageid` int(11) NOT NULL,
  `tagid` int(11) NOT NULL,
  KEY `imageid` (`imageid`,`tagid`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `imageid` int(11) NOT NULL,
  `cnt` int(11) NOT NULL,
  UNIQUE KEY `imageid_2` (`imageid`),
  KEY `cnt` (`cnt`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `likesusers`
--

DROP TABLE IF EXISTS `likesusers`;
CREATE TABLE IF NOT EXISTS `likesusers` (
  `imageid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  KEY `imageid` (`imageid`,`userid`)
) TYPE=MyISAM;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `tagid` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(128) binary NOT NULL,
  PRIMARY KEY (`tagid`),
  UNIQUE KEY `tag` (`tag`)
) TYPE=MyISAM  AUTO_INCREMENT=68843 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(128) binary NOT NULL,
  PRIMARY KEY (`userid`)
) TYPE=MyISAM  AUTO_INCREMENT=1000000 ;

