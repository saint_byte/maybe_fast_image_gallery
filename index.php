<?php
include('template/header.php');

include('classes/autoload.php');
$ims = new Images();
$page = intval($_GET['page']);
if ($page > 0 ) $page--;
$start = 0;
$per   = 20;
$offset = $page*$per;
$allow_tags = array();
$ignore_tag = array();
if ($_GET['tags'] != '') $allow_tags = array($_GET['tags']);
if ($_GET['ignore_tags'] != '')  $ignore_tag = array($_GET['ignore_tags']);
$sort_by_date = false;
$sort_by_likes = false;
if ($_GET['sort_by_date'] =='1')
{
    $sort_by_date =  true;
}
if ($_GET['sort_by_likes'] =='1')
{
    $sort_by_likes =  true;
}

if (strpos($_GET['tags'],',')) 
{
    $allow_tags = explode(',',$_GET['tags']);
}
if (strpos($_GET['ignore_tags'],',')) 
{
    $ignore_tag = explode(',',$_GET['ignore_tags']);
}

$images_cnt = $ims->getCountByParams($allow_tags,$ignore_tag);

$images_arr = $ims->getListByParams($offset,$per,$allow_tags,$ignore_tag,$sort_by_date,$sort_by_likes);
foreach($images_arr as $image)
{
    print '<a href="view.php?imageid='.$image['imageid'].'"><img src="'.$image['src'].'" /></a>';

}
print 'Всего:'.$images_cnt.'<br />';
print 'С:'.$offset.' по:'.($offset+$per).'<br />';

print '<div class="pages_scroll">';
for($page_i = 1; $page_i*$per < $images_cnt+$per; $page_i++ )
{
  if ($page_i == ($page+1)) 
  {
  print $page_i.' ';
  }
  else
  {
   print '<a href="?tags='.join($allow_tags,',').'&ignore_tags='.join($ignore_tags,',').'&sort_by_date='.($sort_by_date ? '1':'0'  ).'&sort_by_likes='.($sort_by_likes ? '1':'0'  ).'&page='.$page_i.'">'.$page_i.'</a> ';
  }
}
print '</div>';
include('template/footer.php');
